import { WebPlugin } from '@capacitor/core';
import type {
  CreateSourceOptions,
  CreateTokenOptions,
  OmisePlugin,
  OmiseSource,
  OmiseToken,
} from './definitions';

declare var Omise: any;

export class OmiseWeb extends WebPlugin implements OmisePlugin {
  omise: any;

  async loadOmiseJS(): Promise<void> {
    const scriptEl: HTMLScriptElement = document.createElement('script');
    scriptEl.src = 'https://cdn.omise.co/omise.js';
    document.body.appendChild(scriptEl);

    return new Promise((resolve, reject) => {
      scriptEl.addEventListener(
        'error',
        (ev: ErrorEvent) => {
          document.body.removeChild(scriptEl);
          reject('Failed to load Omise JS: ' + ev.message);
        },
        { once: true },
      );

      scriptEl.addEventListener(
        'load',
        () => {
          try {
            this.omise = Omise;
            resolve();
          } catch (err) {
            document.body.removeChild(scriptEl);
            reject(err);
          }
        },
        { once: true },
      );
    });
  }

  async setPublicKey(options: { publicKey: string }): Promise<void> {
    if (
      typeof options.publicKey !== 'string' ||
      options.publicKey.trim().length === 0
    ) {
      throw new Error('you must provide a valid key');
    }

    if (!this.omise) {
      await this.loadOmiseJS();
    }

    this.omise.setPublicKey(options.publicKey);
  }

  async createToken(options: CreateTokenOptions): Promise<OmiseToken> {
    return new Promise<OmiseToken>((resolve, reject) => {
      this.omise.createToken(
        'card',
        options,
        (statusCode: number, response: any) => {
          if (statusCode === 200) resolve(response);
          else reject(response);
        },
      );
    });
  }

  async createSource(options: CreateSourceOptions): Promise<OmiseSource> {
    return new Promise<OmiseSource>((resolve, reject) => {
      this.omise.createSource(
        options.type,
        options,
        (statusCode: number, response: any) => {
          if (statusCode === 200) resolve(response);
          else reject(response);
        },
      );
    });
  }
}
