import { registerPlugin } from '@capacitor/core';

import type { OmisePlugin } from './definitions';

const Omise = registerPlugin<OmisePlugin>('Omise', {
  web: () => import('./web').then(m => new m.OmiseWeb()),
});

export * from './definitions';
export { Omise };
