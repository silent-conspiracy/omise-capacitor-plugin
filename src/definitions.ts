export type OmiseChargeStatus =
  | 'failed'
  | 'expired'
  | 'pending'
  | 'reversed'
  | 'successful';

export interface CreateTokenOptions {
  name: string; // name on card
  number: string; // card number
  expiration_month: number;
  expiration_year: number;
  security_code: string;
}

export interface OmiseToken {
  object: string;
  id: string;
  livemode: boolean;
  location: string;
  card: OmiseCard;
  charge_status: OmiseChargeStatus;
  created_at: string;
  used: boolean;
}

export interface OmiseCard {
  object: string;
  id: string;
  livemode: boolean;
  bank: string;
  brand: string;
  city: string;
  country: string;
  created_at: string;
  expiration_month: number;
  expiration_year: number;
  financing: string;
  fingerprint: string;
  last_digits: string;
  name: string;
  postal_code: string;
  security_code_check: boolean;
}

export interface CreateSourceOptions {
  type: 'paynow';
  currency: string;
  amount: number;
}

export interface OmiseSource {
  object: string;
  id: string;
  type: string;
  amount: number;
  currency: string;
  flow: string;

  // somehow omise-ios sdk does not provide these fields
  livemode?: boolean;
  location?: string;
  barcode?: string;
  charge_status?: OmiseChargeStatus;
  created_at?: string;
  email?: string;
  installment_term?: number;
  mobile_number?: string;
  name?: string;
  phone_number?: string;
  references?: OmiseReferences;
  scannable_code?: OmiseBarcode;
  store_id?: string;
  store_name?: string;
  terminal_id?: string;
  zero_interest_installments?: boolean;
}

export interface OmiseReferences {
  barcode: string
  customer_amount: number
  customer_currency: string
  customer_exchange_rate: number
  device_id: string
  expires_at: string
  omise_tax_id: string
  payment_code: string
  reference_number1: string
  reference_number2: string
  va_code: string
}

export interface OmiseBarcode {
  object: string
  id: string
  livemode: boolean
  location: string
  type: string
  image: OmiseDocument
  created_at: string
  deleted: boolean
}

export interface OmiseDocument {
  object: string
  id: string
  livemode: boolean
  location: string
  filename: string
  download_uri: string
  created_at: string
  deleted: boolean
}
export interface OmisePlugin {
  setPublicKey(options: { publicKey: string }): Promise<void>;
  createToken(options: CreateTokenOptions): Promise<OmiseToken>;
  createSource(options: CreateSourceOptions): Promise<OmiseSource>;
}
