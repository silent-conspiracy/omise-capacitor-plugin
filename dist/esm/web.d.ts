import { WebPlugin } from '@capacitor/core';
import type { CreateSourceOptions, CreateTokenOptions, OmisePlugin, OmiseSource, OmiseToken } from './definitions';
export declare class OmiseWeb extends WebPlugin implements OmisePlugin {
    omise: any;
    loadOmiseJS(): Promise<void>;
    setPublicKey(options: {
        publicKey: string;
    }): Promise<void>;
    createToken(options: CreateTokenOptions): Promise<OmiseToken>;
    createSource(options: CreateSourceOptions): Promise<OmiseSource>;
}
