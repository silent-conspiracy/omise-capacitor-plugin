import type { OmisePlugin } from './definitions';
declare const Omise: OmisePlugin;
export * from './definitions';
export { Omise };
