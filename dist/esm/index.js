import { registerPlugin } from '@capacitor/core';
const Omise = registerPlugin('Omise', {
    web: () => import('./web').then(m => new m.OmiseWeb()),
});
export * from './definitions';
export { Omise };
//# sourceMappingURL=index.js.map