import { WebPlugin } from '@capacitor/core';
export class OmiseWeb extends WebPlugin {
    async loadOmiseJS() {
        const scriptEl = document.createElement('script');
        scriptEl.src = 'https://cdn.omise.co/omise.js';
        document.body.appendChild(scriptEl);
        return new Promise((resolve, reject) => {
            scriptEl.addEventListener('error', (ev) => {
                document.body.removeChild(scriptEl);
                reject('Failed to load Omise JS: ' + ev.message);
            }, { once: true });
            scriptEl.addEventListener('load', () => {
                try {
                    this.omise = Omise;
                    resolve();
                }
                catch (err) {
                    document.body.removeChild(scriptEl);
                    reject(err);
                }
            }, { once: true });
        });
    }
    async setPublicKey(options) {
        if (typeof options.publicKey !== 'string' ||
            options.publicKey.trim().length === 0) {
            throw new Error('you must provide a valid key');
        }
        if (!this.omise) {
            await this.loadOmiseJS();
        }
        this.omise.setPublicKey(options.publicKey);
    }
    async createToken(options) {
        return new Promise((resolve, reject) => {
            this.omise.createToken('card', options, (statusCode, response) => {
                if (statusCode === 200)
                    resolve(response);
                else
                    reject(response);
            });
        });
    }
    async createSource(options) {
        return new Promise((resolve, reject) => {
            this.omise.createSource(options.type, options, (statusCode, response) => {
                if (statusCode === 200)
                    resolve(response);
                else
                    reject(response);
            });
        });
    }
}
//# sourceMappingURL=web.js.map