import Foundation
import Capacitor
import OmiseSDK

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(OmisePlugin)
public class OmisePlugin: CAPPlugin {
    private var client: OmiseSDK.Client?

    @objc public func setPublicKey(_ call: CAPPluginCall) {
        let publicKey = call.getString("publicKey")
        if (publicKey != nil && !(publicKey?.isEmpty ?? true)) {
            client = OmiseSDK.Client.init(publicKey: publicKey!)
            call.resolve()
        } else {
            client = nil
            call.resolve()
        }
    }
    
    @objc public func createToken(_ call: CAPPluginCall) {
        guard client != nil else {
            call.reject("Public Key was not set.")
            return
        }
        guard let dataName = call.getString("name") else {
            call.reject("name must be provided.")
            return
        }
        guard let dataNumber = call.getString("number") else {
            call.reject("number must be provided.")
            return
        }
        guard let dataExpirationMonth = call.getInt("expiration_month") else {
            call.reject("expiration_month must be provided.")
            return
        }
        guard let dataExpirationYear = call.getInt("expiration_year") else {
            call.reject("expiration_year must be provided.")
            return
        }
        guard let dataSecurityCode = call.getString("security_code") else {
            call.reject("security_code must be provided.")
            return
        }
        let tokenParameter = Token.CreateParameter(
            name: dataName,
            number: dataNumber,
            expirationMonth: dataExpirationMonth,
            expirationYear: dataExpirationYear,
            securityCode: dataSecurityCode
        )
        let request = Request<Token>(parameter: tokenParameter)
        let requestTask = client!.requestTask(with: request, completionHandler: {(result: RequestResult<Token>) in
            switch(result) {
            case .success(let token):
                var data: [String: Any?] = [
                    "object": token.object,
                    "id": token.id,
                    "livemode": token.isLiveMode,
                    "location": token.location,
                    "card": nil,
                    "charge_status": token.chargeStatus.rawValue,
                    "created_at": token.createdDate,
                    "used": token.isUsed
                ]
                if token.card != nil {
                    let card = token.card!
                    data["card"] = [
                        "object": card.object,
                        "id": card.id,
                        "livemode": card.isLiveMode,
                        "bank": card.bankName as Any,
                        "brand": card.brand as Any,
                        "city": card.city as Any,
                        "country": card.countryCode as Any,
                        "created_at": card.createdDate,
                        "expiration_month": card.expirationMonth as Any,
                        "expiration_year": card.expirationYear as Any,
                        "financing": card.financing as Any,
                        "fingerprint": card.fingerprint as Any,
                        "last_digits": card.lastDigits,
                        "name": card.name as Any,
                        "postal_code": card.postalCode as Any,
                        "security_code_check": card.securityCodeCheck
                    ]
                }
                call.resolve(data as PluginCallResultData)
            case .failure(let error):
                call.reject(error.localizedDescription)
            }
        })
        requestTask.resume()
    }
    
    @objc public func createSource(_ call: CAPPluginCall) {
        guard client != nil else {
            call.reject("Public Key was not set.")
            return
        }
        guard let type = call.getString("type") else {
            call.reject("type must be provided.")
            return
        }
        guard let currency = call.getString("currency") else {
            call.reject("currency must be provided.")
            return
        }
        guard let amount = call.options["amount"] as? Int64 else {
            call.reject("amount must be provided.")
            return
        }
        var paymentInfo: PaymentInformation
        switch type {
        case "paynow":
            paymentInfo = PaymentInformation.paynow
        default:
            paymentInfo = PaymentInformation.paynow
        }
        let sourceParameter = CreateSourceParameter(
            paymentInformation: paymentInfo,
            amount: amount,
            currency: Currency(code: currency)
        )
        let request = Request<Source>(parameter: sourceParameter)
        let requestTask = client!.requestTask(with: request, completionHandler: {(result: RequestResult<Source>) in
            switch(result) {
            case .success(let source):
                let data: [String: Any?] = [
                    "object": source.object,
                    "id": source.id,
                    "type": source.paymentInformation.sourceType,
                    "amount": source.amount,
                    "currency": source.currency.code,
                    "flow": source.flow.rawValue
                ]
                call.resolve(data as PluginCallResultData)
            case .failure(let error):
                call.reject(error.localizedDescription)
            }
        })
        requestTask.resume()
    }
}
