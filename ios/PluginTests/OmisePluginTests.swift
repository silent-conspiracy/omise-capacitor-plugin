import XCTest
import Capacitor
@testable import Plugin

class OmiseTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testEcho() {
        // This is an example of a functional test case for a plugin.
        // Use XCTAssert and related functions to verify your tests produce the correct results.

        let plugin = OmisePlugin()
        let pluginCall = CAPPluginCall(
            callbackId: "123",
            options: JSTypes.coerceDictionaryToJSObject([
                "publicKey": "pkey_test_5kh6p3bjtmaav1qtz1i"
            ]),
            success: { (result: CAPPluginCallResult?, pluginCall: CAPPluginCall?) in
                print(result);
            }, error: { (error: CAPPluginCallError?) in
                print(error);
            })
        
        plugin.perform("createToken", with: pluginCall)

        //XCTAssertEqual(value, result)
    }
}
