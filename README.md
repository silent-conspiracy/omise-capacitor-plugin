# @capacitor-community/omise

Omise SDK bindings for Capacitor Applications

## Install

```bash
npm install @capacitor-community/omise
npx cap sync
```

## Update OmiseSDK
Follow instructions in https://github.com/omise/omise-ios/tree/v4.3.2#carthage
```
cd ios
carthage update --use-xcframeworks --no-use-binaries
cd ..
git add -f ios/Carthage/Build
```
Commit ios/Carthage/Build folder
Check step 8 and 9 of the "To install a different Xcode version" section.

To use a different Xcode version:
```
xcode-select -p
sudo xcode-select -s path-to-xcode-you-would-like-to-use/Contents/Developer
```

To install a different Xcode version:
1. Go to https://developer.apple.com/downloads/
2. Download the desired XCode version .xip file
3. Extract the .xip file anywhere
4. Rename the extracted XCode file, to the specific version
5. Move the XCode file into Applications folder.
6. Install the ios simulator / development libraries for the specific xcode version.
7. If carthage build fails, try to manually "Archive" the omise-sdk github repository by opening it on the new XCode version and resolve the issues there.
8. Open ios/Carthage/Build/OmiseSDK.xcframework/ios-arm64 with finder
9. Open Info.plist and ensure that CFBundleShortVersionString (Bundle Version Number (string)) is set.

## API

<docgen-index>

* [`setPublicKey(...)`](#setpublickey)
* [`createToken(...)`](#createtoken)
* [`createSource(...)`](#createsource)
* [Interfaces](#interfaces)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### setPublicKey(...)

```typescript
setPublicKey(options: { publicKey: string; }) => Promise<void>
```

| Param         | Type                                |
| ------------- | ----------------------------------- |
| **`options`** | <code>{ publicKey: string; }</code> |

--------------------


### createToken(...)

```typescript
createToken(options: CreateTokenOptions) => Promise<OmiseToken>
```

| Param         | Type                                                              |
| ------------- | ----------------------------------------------------------------- |
| **`options`** | <code><a href="#createtokenoptions">CreateTokenOptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#omisetoken">OmiseToken</a>&gt;</code>

--------------------


### createSource(...)

```typescript
createSource(options: CreateSourceOptions) => Promise<OmiseSource>
```

| Param         | Type                                                                |
| ------------- | ------------------------------------------------------------------- |
| **`options`** | <code><a href="#createsourceoptions">CreateSourceOptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#omisesource">OmiseSource</a>&gt;</code>

--------------------


### Interfaces


#### OmiseToken

| Prop                | Type                                                                          |
| ------------------- | ----------------------------------------------------------------------------- |
| **`object`**        | <code>string</code>                                                           |
| **`id`**            | <code>string</code>                                                           |
| **`livemode`**      | <code>boolean</code>                                                          |
| **`location`**      | <code>string</code>                                                           |
| **`card`**          | <code><a href="#omisecard">OmiseCard</a></code>                               |
| **`charge_status`** | <code>"failed" \| "expired" \| "pending" \| "reversed" \| "successful"</code> |
| **`created_at`**    | <code>string</code>                                                           |
| **`used`**          | <code>boolean</code>                                                          |


#### OmiseCard

| Prop                      | Type                 |
| ------------------------- | -------------------- |
| **`object`**              | <code>string</code>  |
| **`id`**                  | <code>string</code>  |
| **`livemode`**            | <code>boolean</code> |
| **`bank`**                | <code>string</code>  |
| **`brand`**               | <code>string</code>  |
| **`city`**                | <code>string</code>  |
| **`country`**             | <code>string</code>  |
| **`created_at`**          | <code>string</code>  |
| **`expiration_month`**    | <code>number</code>  |
| **`expiration_year`**     | <code>number</code>  |
| **`financing`**           | <code>string</code>  |
| **`fingerprint`**         | <code>string</code>  |
| **`last_digits`**         | <code>string</code>  |
| **`name`**                | <code>string</code>  |
| **`postal_code`**         | <code>string</code>  |
| **`security_code_check`** | <code>boolean</code> |


#### CreateTokenOptions

| Prop                   | Type                |
| ---------------------- | ------------------- |
| **`name`**             | <code>string</code> |
| **`number`**           | <code>string</code> |
| **`expiration_month`** | <code>number</code> |
| **`expiration_year`**  | <code>number</code> |
| **`security_code`**    | <code>string</code> |


#### OmiseSource

| Prop                             | Type                                                                          |
| -------------------------------- | ----------------------------------------------------------------------------- |
| **`object`**                     | <code>string</code>                                                           |
| **`id`**                         | <code>string</code>                                                           |
| **`type`**                       | <code>string</code>                                                           |
| **`amount`**                     | <code>number</code>                                                           |
| **`currency`**                   | <code>string</code>                                                           |
| **`flow`**                       | <code>string</code>                                                           |
| **`livemode`**                   | <code>boolean</code>                                                          |
| **`location`**                   | <code>string</code>                                                           |
| **`barcode`**                    | <code>string</code>                                                           |
| **`charge_status`**              | <code>"failed" \| "expired" \| "pending" \| "reversed" \| "successful"</code> |
| **`created_at`**                 | <code>string</code>                                                           |
| **`email`**                      | <code>string</code>                                                           |
| **`installment_term`**           | <code>number</code>                                                           |
| **`mobile_number`**              | <code>string</code>                                                           |
| **`name`**                       | <code>string</code>                                                           |
| **`phone_number`**               | <code>string</code>                                                           |
| **`references`**                 | <code><a href="#omisereferences">OmiseReferences</a></code>                   |
| **`scannable_code`**             | <code><a href="#omisebarcode">OmiseBarcode</a></code>                         |
| **`store_id`**                   | <code>string</code>                                                           |
| **`store_name`**                 | <code>string</code>                                                           |
| **`terminal_id`**                | <code>string</code>                                                           |
| **`zero_interest_installments`** | <code>boolean</code>                                                          |


#### OmiseReferences

| Prop                         | Type                |
| ---------------------------- | ------------------- |
| **`barcode`**                | <code>string</code> |
| **`customer_amount`**        | <code>number</code> |
| **`customer_currency`**      | <code>string</code> |
| **`customer_exchange_rate`** | <code>number</code> |
| **`device_id`**              | <code>string</code> |
| **`expires_at`**             | <code>string</code> |
| **`omise_tax_id`**           | <code>string</code> |
| **`payment_code`**           | <code>string</code> |
| **`reference_number1`**      | <code>string</code> |
| **`reference_number2`**      | <code>string</code> |
| **`va_code`**                | <code>string</code> |


#### OmiseBarcode

| Prop             | Type                                                    |
| ---------------- | ------------------------------------------------------- |
| **`object`**     | <code>string</code>                                     |
| **`id`**         | <code>string</code>                                     |
| **`livemode`**   | <code>boolean</code>                                    |
| **`location`**   | <code>string</code>                                     |
| **`type`**       | <code>string</code>                                     |
| **`image`**      | <code><a href="#omisedocument">OmiseDocument</a></code> |
| **`created_at`** | <code>string</code>                                     |
| **`deleted`**    | <code>boolean</code>                                    |


#### OmiseDocument

| Prop               | Type                 |
| ------------------ | -------------------- |
| **`object`**       | <code>string</code>  |
| **`id`**           | <code>string</code>  |
| **`livemode`**     | <code>boolean</code> |
| **`location`**     | <code>string</code>  |
| **`filename`**     | <code>string</code>  |
| **`download_uri`** | <code>string</code>  |
| **`created_at`**   | <code>string</code>  |
| **`deleted`**      | <code>boolean</code> |


#### CreateSourceOptions

| Prop           | Type                  |
| -------------- | --------------------- |
| **`type`**     | <code>"paynow"</code> |
| **`currency`** | <code>string</code>   |
| **`amount`**   | <code>number</code>   |

</docgen-api>


## Changelog
- v0.0.2:
  - Rebuild OmiseSDK.xcframework with Carthage 0.38.0
- v0.0.1:
  - Initial release