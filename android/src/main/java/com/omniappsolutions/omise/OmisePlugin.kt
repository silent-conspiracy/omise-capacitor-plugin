package com.omniappsolutions.omise

import co.omise.android.api.Client
import co.omise.android.api.RequestListener
import co.omise.android.models.CardParam
import co.omise.android.models.Source
import co.omise.android.models.SourceType
import co.omise.android.models.Token
import com.getcapacitor.JSObject
import com.getcapacitor.Plugin
import com.getcapacitor.PluginCall
import com.getcapacitor.PluginMethod
import com.getcapacitor.annotation.CapacitorPlugin

@CapacitorPlugin(name = "Omise")
class OmisePlugin : Plugin() {
    private var client: Client? = null

    @PluginMethod
    fun setPublicKey(call: PluginCall) {
        val publicKey = call.getString("publicKey")
        if (publicKey != null && publicKey != "") {
            this.client = Client(publicKey)
            call.resolve()
        } else {
            this.client = null
            call.resolve()
        }
    }

    @PluginMethod
    fun createToken(call: PluginCall) {
        if (this.client == null) {
            call.reject("Public Key was not set.")
            return
        }
        val dataName = call.getString("name") ?: run {
            call.reject("name must be provided.")
            return@createToken
        }
        val dataNumber = call.getString("number") ?: run {
            call.reject("number must be provided.")
            return@createToken
        }
        val dataExpirationMonth = call.getInt("expiration_month") ?: run {
            call.reject("expiration_month must be provided.")
            return@createToken
        }
        val dataExpirationYear = call.getInt("expiration_year") ?: run {
            call.reject("expiration_year must be provided.")
            return@createToken
        }
        val dataSecurityCode = call.getString("security_code") ?: run {
            call.reject("security_code must be provided.")
            return@createToken
        }

        val cardParam = CardParam(
                name = dataName,
                number = dataNumber,
                expirationMonth = dataExpirationMonth,
                expirationYear = dataExpirationYear,
                securityCode = dataSecurityCode
        )
        val request = Token.CreateTokenRequestBuilder(cardParam).build()

        this.client!!.send(request, object : RequestListener<Token>{
            override fun onRequestSucceed(model: Token) {
                val card = if (model.card != null) {
                    val modelCard = model.card!!;
                    val cardObject = JSObject()
                    cardObject.put("object", modelCard.modelObject)
                    cardObject.put("id", modelCard.id)
                    cardObject.put("livemode", modelCard.livemode)
                    cardObject.put("bank", modelCard.bank)
                    cardObject.put("brand", modelCard.brand)
                    cardObject.put("city", modelCard.city)
                    cardObject.put("country", modelCard.country)
                    cardObject.put("created_at", modelCard.created)
                    cardObject.put("expiration_month", modelCard.expirationMonth)
                    cardObject.put("expiration_year", modelCard.expirationYear)
                    cardObject.put("financing", modelCard.financing)
                    cardObject.put("fingerprint", modelCard.fingerprint)
                    cardObject.put("last_digits", modelCard.lastDigits)
                    cardObject.put("name", modelCard.name)
                    cardObject.put("postal_code", modelCard.postalCode)
                    cardObject.put("security_code_check", modelCard.securityCodeCheck)
                    cardObject
                } else null
                val ret = JSObject()
                ret.put("object", model.modelObject)
                ret.put("id", model.id)
                ret.put("livemode", model.livemode)
                ret.put("location", model.location)
                ret.put("card", card)
                ret.put("charge_status", model.chargeStatus.value)
                ret.put("created_at", model.created)
                ret.put("used", model.used)
                call.resolve(ret)
            }

            override fun onRequestFailed(throwable: Throwable) {
                call.reject(throwable.localizedMessage)
            }
        })
    }

    @PluginMethod
    fun createSource(call: PluginCall) {
        if (this.client == null) {
            call.reject("Public Key was not set.")
            return
        }
        val type = call.getString("type") ?: run {
            call.reject("type must be provided.")
            return@createSource
        }
        val currency = call.getString("currency") ?: run {
            call.reject("currency must be provided.")
            return@createSource
        }
        val amount = call.getFloat("amount")?.toLong() ?: run {
            call.reject("amount must be provided.")
            return@createSource
        }

        val sourceType = if (type == "paynow") {
            SourceType.PayNow
        } else {
            SourceType.PayNow
        }

        val request = Source.CreateSourceRequestBuilder(amount, currency, sourceType).build()

        this.client!!.send(request, object : RequestListener<Source>{
            override fun onRequestSucceed(model: Source) {
                val references = if (model.references != null) {
                    val modelRef = model.references!!
                    val referencesObj = JSObject()
                    referencesObj.put("barcode", modelRef.barcode)
                    referencesObj.put("customer_amount", modelRef.customerAmount)
                    referencesObj.put("customer_currency", modelRef.customerCurrency)
                    referencesObj.put("customer_exchange_rate", modelRef.customerExchangeRate)
                    referencesObj.put("device_id", modelRef.deviceId)
                    referencesObj.put("expires_at", modelRef.expiresAt)
                    referencesObj.put("omise_tax_id", modelRef.omiseTaxId)
                    referencesObj.put("payment_code", modelRef.paymentCode)
                    referencesObj.put("reference_number1", modelRef.referenceNumber1)
                    referencesObj.put("reference_number2", modelRef.referenceNumber2)
                    referencesObj.put("va_code", modelRef.vaCode)
                    referencesObj
                } else null
                val scannableCode = if (model.scannableCode != null) {
                    val modelSC = model.scannableCode!!
                    val image = if (modelSC.image != null) {
                        val modelImage = modelSC.image!!
                        val imageObject = JSObject()
                        imageObject.put("object", modelImage.modelObject)
                        imageObject.put("id", modelImage.id)
                        imageObject.put("livemode", modelImage.livemode)
                        imageObject.put("location", modelImage.location)
                        imageObject.put("filename", modelImage.filename)
                        imageObject.put("download_uri", modelImage.downloadUri)
                        imageObject.put("created_at", modelImage.created)
                        imageObject.put("deleted", modelImage.deleted)
                        imageObject
                    } else null
                    val scObject = JSObject()
                    scObject.put("object", modelSC.modelObject)
                    scObject.put("id", modelSC.id)
                    scObject.put("livemode", modelSC.livemode)
                    scObject.put("location", modelSC.location)
                    scObject.put("type", modelSC.type)
                    scObject.put("image", image)
                    scObject.put("created_at", modelSC.created)
                    scObject.put("deleted", modelSC.deleted)
                    scObject
                } else null
                val ret = JSObject()
                ret.put("object", model.modelObject)
                ret.put("id", model.id)
                ret.put("livemode", model.livemode)
                ret.put("location", model.location)
                ret.put("amount", model.amount)
                ret.put("barcode", model.barcode)
                ret.put("charge_status", model.chargeStatus.value)
                ret.put("created_at", model.created)
                ret.put("currency", model.currency)
                ret.put("email", model.email)
                ret.put("flow", model.flow)
                ret.put("installment_term", model.installmentTerm)
                ret.put("mobile_number", model.mobileNumber)
                ret.put("name", model.name)
                ret.put("phone_number", model.phoneNumber)
                ret.put("references", references)
                ret.put("scannable_code", scannableCode)
                ret.put("store_id", model.storeId)
                ret.put("store_name", model.storeName)
                ret.put("terminal_id", model.terminalId)
                ret.put("type", model.type.name)
                ret.put("zero_interest_installments", model.zeroInterestInstallments)
                call.resolve(ret)
            }

            override fun onRequestFailed(throwable: Throwable) {
                call.reject(throwable.localizedMessage)
            }
        })
    }
}